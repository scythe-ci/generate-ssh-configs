module scythe-ci/generate-ssh-configs

go 1.12

require (
	github.com/aws/aws-sdk-go v1.44.194
	github.com/digitalocean/godo v1.84.1
	github.com/spf13/cobra v1.6.1
	golang.org/x/oauth2 v0.4.0
)
